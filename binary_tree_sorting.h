//
// Created by avu on 19.09.2021.
//

#ifndef HPSLAB1_BINARY_TREE_SORTING_H
#define HPSLAB1_BINARY_TREE_SORTING_H

struct stack {
    struct stack* next;
    struct node* node;
} typedef stack_t;

struct node {
    int value;
    struct node* left_child;
    struct node* right_child;
    int left_elements;
} typedef node_t;

void add_node(int value, node_t** tree);

void show_tree(node_t* tree);

int* get_int_massive(node_t* node, int size);


#endif //HPSLAB1_BINARY_TREE_SORTING_H
