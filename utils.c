//
// Created by avu on 25.10.2021.
//


#include "utils.h"


int comp(const int* i, const int* j) {
    return *i - *j;
}

int min_elem(int* list, int* pointers, int range, int rest, int current_threads) {
    int min_elem = 10000;
    int index_min_elem = -1;
    int min_elems[current_threads];
    for (int i = 0; i < current_threads; i ++) {
        int dst = range + range * i + (i == (current_threads - 1) ? rest : 0);
        min_elems[i] = pointers[i] < dst  ? list[pointers[i]] : 10000;
        if (min_elem > min_elems[i]) {
            index_min_elem = i;
            min_elem = min_elems[i];
        }
    }
    pointers[index_min_elem] ++;
    return min_elem;
}

int* copy(int* src, int index_start, int index_end, int size) {
    int* result = malloc(size * sizeof(int));

    for (int i = index_start, j = 0; i < index_end; i ++) {
        result[j++] = src[i];
    }

    return result;
}
