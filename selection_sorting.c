//
// Created by avu on 19.09.2021.
//

#include "selection_sorting.h"


void sort(int* src, int cnt) {

    for (int i = 0; i < cnt; i ++) {
        int max = 0;
        for (int j = 0; j < cnt - i; j ++ ) {
            if (src[max] <= src[j]) {
                max = j;
            }
        }
        int temp = src[cnt - i - 1];
        src[cnt - i - 1] = src[max];
        src[max] = temp;
    }
}

void sort_multi_thread(int* src, int cnt) {
    for (int i = cnt - 1; i > 0; --i) {
        struct Compare max;
        max.value = src[i];
        max.index = i;
#pragma omp parallel for reduction(maximum:max) default(none) shared(src, i)
        for (int j = i - 1; j >= 0; --j) {
            if (src[j] > max.value) {
                max.value = src[j];
                max.index = j;
            }
        }
        int tmp = src[i];
        src[i] = max.value;
        src[max.index] = tmp;
    }
}


