//
// Created by avu on 25.10.2021.
//

#ifndef HPSLAB1_UTILS_H
#define HPSLAB1_UTILS_H

#include <malloc.h>

int* copy(int* src, int index_start, int index_end, int size);
int min_elem(int* list, int* pointers, int range, int rest, int current_threads);
int comp(const int* i, const int* j);

#endif //HPSLAB1_UTILS_H
