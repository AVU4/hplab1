//
// Created by avu on 19.09.2021.
//

#include <stddef.h>
#include <malloc.h>
#include "binary_tree_sorting.h"

void push(stack_t** pStack, node_t* node);
node_t* pop(stack_t** pStack);

void add_node(int value, node_t** tree) {
    if (*tree == NULL) {
        *tree = malloc(sizeof(node_t));
        (*tree)->value = value;
        (*tree)->left_child = NULL;
        (*tree)->right_child = NULL;
        (*tree)->left_elements = 0;
    } else {
        if ( value < (*tree)->value ){
            (*tree)->left_elements ++;
            add_node(value,  &((*tree)->left_child));
        }
        else
            add_node(value, &((*tree)->right_child));
    }
}



void show_tree(node_t* tree) {
    if (tree != NULL) {
        show_tree(tree->left_child);
        printf("%d ", tree->value);
        show_tree(tree->right_child);
    }
}

int* get_int_massive(node_t* tree, int size) {
    int* massive = malloc(size * sizeof(int));
    int flag = 0;
    int index = 0;
    node_t* current = tree;
    stack_t* stack = NULL;

    while (flag == 0) {
        if (current != NULL) {
            push(&stack, current);
            current = current->left_child;
        } else {
            if (!stack == NULL) {
                current = pop(&stack);
                massive[index++] = current->value;
                current = current->right_child;
            } else flag = 1;
        }
    }
    return massive;
}

void push(stack_t** pStack, node_t* node) {
    stack_t* stack = malloc(sizeof(stack_t));

    stack->node = node;
    stack->next = (*pStack);
    (*pStack)  = stack;
}

node_t* pop(stack_t** pStack) {
    node_t* node;
    stack_t* stack;

    stack = *pStack;
    node = stack->node;
    *pStack = stack->next;
    free(stack);
    return node;
}