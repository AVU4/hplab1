//
// Created by avu on 19.09.2021.
//

#ifndef HPSLAB1_SELECTION_SORTING_H
#define HPSLAB1_SELECTION_SORTING_H

#include "stdio.h"
#include "omp.h"

struct Compare {
    int value;
    int index;
};
#pragma omp declare reduction(maximum : struct Compare :  omp_out = omp_in.value > omp_out.value ? omp_in : omp_out)

void sort(int* src, int cnt);
void sort_multi_thread(int* src, int cnt);

#endif //HPSLAB1_SELECTION_SORTING_H
