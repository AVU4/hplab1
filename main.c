#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "binary_tree_sorting.h"
#include "selection_sorting.h"
#include "time.h"
#include "omp.h"

int main(int argc, char* argv[]) {
    int cnt;
    sscanf(argv[1], "%d", &cnt);
    puts("Preparing dataset");
    int* data = malloc(cnt * sizeof(int));
    int* dataset_1 = malloc(cnt * sizeof(int)); //dataset for simple sorting by tree
    int* dataset_2 = malloc(cnt * sizeof(int)); //dataset for sorting in several threads by tree
    int* dataset_3 = malloc(cnt * sizeof(int)); //dataset for sorting in 2 threads by tree
    int* dataset_4 = malloc(cnt * sizeof(int)); //dataset for simple sorting by selection
    int* dataset_5 = malloc(cnt * sizeof(int)); //dataset for sorting in several threads by selection
    int* dataset_6 = malloc(cnt * sizeof(int)); //dataset for sorting in 2 threads by selection
    int* test_dataset  = malloc(cnt * sizeof(int));

    for(int i = 0; i < cnt; i++){
        data[i] = rand() % 100;
    }
    memcpy(dataset_1, data, cnt * sizeof(int));
    memcpy(dataset_2, data, cnt * sizeof(int));
    memcpy(dataset_3, data, cnt * sizeof(int));
    memcpy(dataset_4, data, cnt * sizeof(int));
    memcpy(dataset_5, data, cnt * sizeof(int));
    memcpy(dataset_6, data, cnt * sizeof(int));
    memcpy(test_dataset, data, cnt * sizeof(int));

    qsort(test_dataset, cnt, sizeof(int), (int(*) (const void *, const void *)) comp);
    clock_t t;

    printf("The sorting by binary tree in one thread is starting.\n");
    t = clock();
    node_t* tree = NULL;
    for (int i = 0; i < cnt; i++) {
        add_node(dataset_1[i], &tree);
    }
    int* my_result = get_int_massive(tree, cnt);
    t = clock() - t;

    printf("Sorting %d elements by binary tree in one thread takes %f seconds.\n", cnt, ((double)t)/CLOCKS_PER_SEC);

    if (memcmp(my_result, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    free(my_result);
    free(dataset_1);

    printf("\n");


    int current_thread;
    int range;
    int rest;
    int* result_2 = malloc(cnt * sizeof(int));
    int* pointers;



#pragma omp parallel default(none) shared(t, dataset_2, range, cnt, rest, result_2, pointers, current_thread)
    {
#pragma omp single
        {
            current_thread = omp_get_num_threads();
            printf("The sorting by binary tree in %d threads is starting.\n", current_thread);
            t = clock();
            range = cnt / current_thread;
            rest = cnt % current_thread;
        }
        node_t* tree_2 = NULL;
        int num_thread = omp_get_thread_num();
        for (int i = 0 + range * num_thread; i < range + range * num_thread + (num_thread == (current_thread - 1) ? rest : 0); i ++) {
            add_node(dataset_2[i], &tree_2);
        }
        int* list = get_int_massive(tree_2, range + (num_thread == (current_thread  - 1) ? rest : 0));

        for (int i = 0; i < range + (num_thread == (current_thread - 1) ? rest : 0); i ++) {
            dataset_2[i + range * omp_get_thread_num()] = list[i];
        }
    }

    pointers = malloc(current_thread * sizeof(int));

    for (int i = 0; i < current_thread; i ++) {
        pointers[i] = range * i;
    }

    for (int i = 0; i < cnt; i ++) {
        result_2[i] = min_elem(dataset_2, pointers, range, rest, current_thread);
    }
    t = clock() - t;
    printf("Sorting %d elements by binary tree in %d threads takes %f seconds.\n", cnt, current_thread, ((double)t)/CLOCKS_PER_SEC);

    if (memcmp(result_2, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    printf("\n");

    free(dataset_2);
    free(result_2);

    printf("The sorting by binary tree in 2 threads is starting.\n");
    t = clock();
    node_t* tree_3 = NULL;
    for (int i = 0; i < cnt; i ++) {
        add_node(dataset_3[i], &tree_3);
    }

    int root = tree_3->value;
    int left_size = tree_3->left_elements;
    int right_size = cnt - left_size;
    node_t* tree_left_branch = tree_3->left_child;
    node_t* tree_right_branch = tree_3->right_child;
    dataset_3[left_size] = root;

#pragma omp parallel sections default(none) shared(tree_left_branch, left_size, tree_right_branch, right_size, dataset_3) num_threads(2)
    {
#pragma omp  section
        {
            int* list = get_int_massive(tree_left_branch, left_size);
            for (int i = 0; i < left_size; i ++) {
                dataset_3[i] = list[i];
            }
        }
#pragma omp section
        {
            int* list = get_int_massive(tree_right_branch, right_size);
            for (int i = 0; i < right_size; i ++) {
                dataset_3[left_size + 1 + i] = list[i];
            }
        }
    }

    t = clock() - t;
    printf("Sorting %d elements by binary tree in %d threads takes %f seconds.\n", cnt, 2, ((double)t)/CLOCKS_PER_SEC);

    if (memcmp(dataset_3, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    free(dataset_3);

    printf("\n");

    printf("The sorting by selection in one thread is starting.\n");

    t = clock();
    sort(dataset_4, cnt);
    t = clock() - t;

    printf("Sorting %d elements by selection in one thread takes %f seconds.\n", cnt, ((double ) t)/CLOCKS_PER_SEC);

    if (memcmp(dataset_4, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    free(dataset_4);

    printf("\n");

    for (int i = 0; i < current_thread; i ++) {
        pointers[i] = range * i;
    }
    int* result_3 = malloc(cnt * sizeof(int));

    printf("The sorting by selection in %d threads is starting.\n", current_thread);

    t = clock();

#pragma omp parallel default(none) shared(range, rest, pointers, dataset_5, current_thread)
    {
        int thread_num = omp_get_thread_num();
        int size = range + (thread_num == (current_thread - 1) ? rest : 0);
        int* list = copy(dataset_5,
                         pointers[thread_num],
                         range + range * thread_num + (thread_num == (current_thread - 1) ? rest : 0),
                         size);

        sort(list, size);

        for (int i = 0; i < range + (thread_num == (current_thread - 1) ? rest : 0); i ++) {
            dataset_5[i + range * thread_num] = list[i];
        }
    }

    for (int i = 0; i < cnt; i ++) {
        result_3[i] = min_elem(dataset_5, pointers, range, rest, current_thread);
    }
    t = clock() - t;
    printf("Sorting %d elements by selection in %d threads takes %f seconds.\n", cnt, current_thread, ((double)t)/CLOCKS_PER_SEC);

    if (memcmp(result_3, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    free(dataset_5);
    free(result_3);

    printf("\n");

    printf("The sorting by selection in 2 threads is starting.\n");

    t = clock();
    sort_multi_thread(dataset_6, cnt);
    t = clock() - t;

    printf("Sorting %d elements by selection in 2 threads takes %f seconds.\n", cnt, ((double ) t)/CLOCKS_PER_SEC);

    if (memcmp(dataset_6, test_dataset, cnt * sizeof(int)) == 0) {
        printf("Result is correct\n");
    } else {
        printf("Result isn't correct\n");
    }

    free(dataset_6);

}